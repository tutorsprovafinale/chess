package it.polimi.chess.data;

public class Scacchiera {

	private Casella[][] caselle;
	
	private static final int SIZE = 8;

	public Scacchiera() {
		super();
		caselle = new Casella[SIZE][SIZE];
		
		for(int r=0; r<SIZE; r++)
			for(int c=0; c<SIZE; c++)
				caselle[r][c] = new Casella(r,c);
		
		caselle[0][4].setPezzo(new Re(Colore.Nero, caselle[0][4]));
	}

	@Override
	public String toString() {
		StringBuilder toRet = new StringBuilder("");
		
		toRet.append("------------------------\n");
		for(int r=0; r<SIZE; r++){
			toRet.append("|");
			for(int c=0;c<SIZE; c++){				
				toRet.append(caselle[r][c].toString()).append("|");				
			}
			toRet.append("\n------------------------\n");
		}
		return toRet.toString();
	}
	
	public void muovi(Coordinate ci, Coordinate cf){
		if(ci == null)
			throw new IllegalArgumentException("the parameter 'ci' cannot be null");
		if(cf == null)
			throw new IllegalArgumentException("the parameter 'cf' cannot be null");
		if(ci.getX()>=0 && ci.getX() < SIZE &&
				ci.getY()>=0 && ci.getY() < SIZE &&
				cf.getX()>=0 && cf.getX() < SIZE &&
				cf.getY()>=0 && cf.getY() < SIZE){
			
			Casella initial = caselle[ci.getX()][ci.getY()];
			Casella fin = caselle[cf.getX()][cf.getY()];
			if(!initial.isEmpty()){
				Pezzo p = initial.getPezzo();
				if(p.validMove(fin)){
					initial.unsetPezzo();
					fin.setPezzo(p);
				}
			}
		}
	}
	
	
	
	
}
