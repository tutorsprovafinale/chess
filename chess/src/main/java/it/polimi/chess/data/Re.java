package it.polimi.chess.data;

public class Re extends Pezzo {

	public Re(Colore colore, Casella casella) {
		super(colore, casella);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean validMove(Casella finalCasella) {
		if(finalCasella == null)
			throw new IllegalArgumentException("Il parametro 'finalCasella' cannot be null.");
		if(!finalCasella.isEmpty() && 
				finalCasella.getPezzo().getColore() == this.colore)
			return false;
		return Math.abs(this.casella.getX()-finalCasella.getX()) <= 1 &&
				Math.abs(this.casella.getY()-finalCasella.getY()) <= 1;
	}

	@Override
	protected Pezzo clone() {
		return new Re(this.colore, this.casella);
	}

	@Override
	public String toString() {
		return "K ";
	}

	
}
