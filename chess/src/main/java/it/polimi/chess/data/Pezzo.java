package it.polimi.chess.data;

public abstract class Pezzo implements Cloneable{

	protected Colore colore;
	
	protected Casella casella;

	public Pezzo(Colore colore, Casella casella) {
		super();
		if(colore == null) 
			throw new IllegalArgumentException("The parameter 'colore' cannot be null");
		if(casella == null) 
			throw new IllegalArgumentException("The parameter 'pezzo' cannot be null");
		this.colore = colore;
		this.casella = casella;
	}
	
	public abstract boolean validMove(Casella finalCasella);

	public Colore getColore() {
		return colore;
	}

	public Casella getCasella() {
		return casella;
	}

	@Override
	protected abstract Pezzo clone();
	
	
	
}
