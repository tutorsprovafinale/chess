package it.polimi.chess.data;

public class Casella extends Coordinate {

	private Pezzo pezzo;
	
	public Casella(int x, int y) {
		
		super(x, y);
		// TODO Auto-generated constructor stub
	}

	public Pezzo getPezzo() {
		if(pezzo != null)
			return pezzo.clone();
		return null;
	}

	public void setPezzo(Pezzo pezzo) {
		if(pezzo == null)
			throw new IllegalArgumentException("The parameter 'pezzo' cannot be null");
		this.pezzo = pezzo.clone();
	}
	
	public void unsetPezzo(){
		this.pezzo = null;
	}
	
	public boolean isEmpty(){
		return pezzo == null;
	}

	@Override
	public String toString() {
		return pezzo == null ? "  " : pezzo.toString();
	}
}
