package it.polimi.chess;

import it.polimi.chess.data.Coordinate;
import it.polimi.chess.data.Scacchiera;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	Scacchiera s = new Scacchiera();
    	System.out.println(s.toString());
    	s.muovi(new Coordinate(0, 4), new Coordinate(1, 5));
    	System.out.println(s.toString());
    	s.muovi(new Coordinate(1, 5), new Coordinate(5, 5));
    	System.out.println(s.toString());

    }
}
